import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'stx-playlists',
  template: `<div class="row">

      <div class="col">
        <stx-card title="Playlists">

          <div class="footer"> * pick your playlist! </div>

          <stx-playlists-list [playlists]="playlists"
            (select)="selected=$event">
          </stx-playlists-list>
        </stx-card>
      </div>

      <div class="col">
       <stx-card title="Playlist {{selected.name}}">
          <stx-playlist-detail [playlist]="selected">
          </stx-playlist-detail>
        </stx-card>
      </div>

    </div> `,
  styles: []
})
export class PlaylistsComponent implements OnInit {


  playlists = [
    {
      name: "Angular greatest Hits!",
      color: '#ff0000',
      favourite: false
    },
    {
      name: "The best of jQuery, vol.3!",
      color: '#00ff00',
      favourite: false
    }
  ]


  selected;

  constructor() { 
    this.selected = this.playlists[0]
  }

  ngOnInit() {
  }

}
