import { Component, OnInit, ViewEncapsulation, Input, ViewChild, ViewChildren, QueryList, Renderer, 
Output, EventEmitter, AfterViewInit, ElementRef } from '@angular/core';
import { PlaylistItemComponent } from './playlist-item.component'


@Component({
  selector: 'stx-playlists-list',
  template: `
    <ul class="list-group">
      <li
        #itemref
        (activate)="select($event)"
        [class.active]="playlist == selected"
        class="list-group-item stx-playlist-item"
        *ngFor="let playlist of playlists; trackBy:$index "
        [data]="playlist"></li>
    </ul>  
    <ng-content></ng-content>
  `,
  styles:[ ],
  providers:[],
  viewProviders:[]
})
export class PlaylistsListComponent implements OnInit, AfterViewInit {

  //@ViewChild('itemref',{ read: ElementRef})
  //@ViewChild(PlaylistItemComponent)
  @ViewChildren(PlaylistItemComponent)
  items:QueryList<PlaylistItemComponent>;
 

  @Output('select')
  selection = new EventEmitter();

  select(playlist){
    this.selected = playlist;
    this.selection.emit(this.selected)
  }

  ngAfterViewInit(){
    // console.log(this.items.changes.subscribe() => {

    // })
  }

  @Input()
  selected;

  @Input()
  playlists = [];

  constructor(private elem:ElementRef, private renderer:Renderer) {
    //console.log(renderer.setElementStyle(elem.nativeElement,'border','1px solid red'))
  }

  ngOnInit() {
  }

}
