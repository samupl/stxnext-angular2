import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'stx-playlist-detail',
  template: `
    <button (click)="edit = !edit">Show/Edit</button>

    <div [ngSwitch]="edit">
      <p *ngSwitchDefault>
        {{playlist.name}}
        {{playlist.favourite}}        
      </p>

      <p *ngSwitchCase="true">
        
        <label [attr.for]=" 'some_id' ">Name</label>
        <input [attr.id]=" 'some_id' "  [readOnly]="false" type="text"  [(ngModel)]="playlist.name">
        <br/>
        <label>Favourite</label>
        <input type="checkbox"  [(ngModel)]="playlist.favourite">
      </p>
      <button>Save</button>
      <button>Cancel</button>
    </div>
  `,
  styles: []
})
export class PlaylistDetailComponent implements OnInit {

  @Input('playlist') set makeCopy(playlist){
    this.playlist = Object.assign({}, playlist);
  }

  playlist;

  constructor() { }

  ngOnInit() {

  }

}
